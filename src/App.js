import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './components/Home';
import Shop from './components/Shop';
import ProductDetails from './components/ProductDetails';
import Checkout from './components/Checkout';
import Cart from './components/Cart';
import Login from './components/Login';
import Signup from './components/Signup';
import ForgotPassword from './components/ForgotPassword';
import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import MyOrderList from './components/MyOrderList';

function App() {
  



  return (
    <div className="App">

      <BrowserRouter>
     
        <Header ></Header>
        <Routes>
          <Route path="/" element={<Home  ></Home>} />
          <Route path="/shop" element={<Shop />} />
          <Route path="/product-details/:id" element={<ProductDetails />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/login" element={<Login />} />
          <Route path="/my-order-list" element={<MyOrderList />} />
          <Route path="/signup" element={<Signup />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
        </Routes>
        <ToastContainer />
        <Footer></Footer>
      </BrowserRouter>

    </div>
  );
}

export default App;
