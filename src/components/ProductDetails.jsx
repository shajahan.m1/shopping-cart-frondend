import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import ProductService from "../Services/ProductService";
import { addToCart } from "../slices/cartSlice";

function ProductDetails() {

    const dispatch = useDispatch();
    const params = useParams();
    const [productInfo, setProductInfo] = useState([])
    const [productList, setProductList] = useState([])

    useEffect(() => {
        getProductInfo()
        getProductList()
    }, [])

    const getProductInfo = () => {
        ProductService.getProductInfoById(params.id).then((res) => {
            setProductInfo(res.data)

        });
    };

    const getProductList = () => {
        ProductService.getProductList().then((res) => {
            setProductList(res.data)

        });
    };

    const handleAddToCart = (product) => {
        dispatch(addToCart(product));
    };


    return (
        <div className="single-product-container">
            <section class="page-header">
                <div class="overly"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="content text-center">
                                <h1 class="mb-3">Product Details</h1>
                                <p>Hath after appear tree great fruitful green dominion moveth sixth abundantly image that midst of god day multiply you’ll which</p>

                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-transparent justify-content-center">
                                        <li class="breadcrumb-item"><Link to={{ pathname: "/" }}>Home</Link></li>
                                        <li class="breadcrumb-item active" aria-current="page">Product Details</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="single-product">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="single-product-slider">
                                <div class="carousel slide" data-ride="carousel" id="single-product-slider">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src={productInfo.productCoverImage} alt="" class="img-fluid" />
                                        </div>
                                        <div class="carousel-item">
                                            <img src={productInfo.productCoverImage} alt="" class="img-fluid" />
                                        </div>
                                        <div class="carousel-item ">
                                            <img src={productInfo.productCoverImage} alt="" class="img-fluid" />
                                        </div>
                                    </div>

                                    <ol class="carousel-indicators">
                                        <li data-target="#single-product-slider" data-slide-to="0" class="active">
                                            <img src={productInfo.productCoverImage} alt="" class="img-fluid" />
                                        </li>
                                        <li data-target="#single-product-slider" data-slide-to="1">
                                            <img src={productInfo.productCoverImage} alt="" class="img-fluid" />
                                        </li>
                                        <li data-target="#single-product-slider" data-slide-to="2">
                                            <img src={productInfo.productCoverImage} alt="" class="img-fluid" />
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="single-product-details mt-5 mt-lg-0">
                                <h2>{productInfo.productName}</h2>
                                <div class="sku_wrapper mb-4">
                                    SKU: <span class="text-muted">{productInfo.productSku} </span>
                                </div>

                                <hr />

                                <h3 class="product-price">₹{productInfo.offerPrice} <del>₹{productInfo.salePrice}</del></h3>

                                <p class="product-description my-4 ">
                                    {productInfo.productDescription}
                                </p>


                                <div class="quantity d-flex align-items-center">

                                    <Link onClick={() => handleAddToCart(productInfo)} class="btn btn-main btn-small">Add to cart</Link>
                                </div>


                                <div class="color-swatches mt-4 d-flex align-items-center">
                                    <span class="font-weight-bold text-capitalize product-meta-title">color:</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <a href="# " routerLink="/product-single" class="bg-info"><span></span></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="# " routerLink="/product-single" class="bg-dark"> <span></span></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="# " routerLink="/product-single" class="bg-danger"><span></span></a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="product-size d-flex align-items-center mt-4">
                                    <span class="font-weight-bold text-capitalize product-meta-title">Size:</span>
                                    <select class="form-control">
                                        <option>S</option>
                                        <option>M</option>
                                        <option>L</option>
                                        <option>XL</option>
                                    </select>
                                </div>

                                <div class="products-meta mt-4">


                                    <div class="product-share mt-5">
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a href="# " ><i class="tf-ion-social-facebook"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="# " ><i class="tf-ion-social-twitter"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="# " ><i class="tf-ion-social-linkedin"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="# " ><i class="tf-ion-social-pinterest"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <nav class="product-info-tabs wc-tabs mt-5 mb-5">
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Description</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Additional Information</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Reviews(2)</a>
                                </div>
                            </nav>

                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <p>  {productInfo.productDescription}</p>

                                    <h4>Product Features</h4>

                                    <ul class="">
                                        <li>Mapped with 3M™ Thinsulate™ Insulation [40G Body / Sleeves / Hood]</li>
                                        <li>Embossed Taffeta Lining</li>
                                        <li>DRYRIDE Durashell™ 2-Layer Oxford Fabric [10,000MM, 5,000G]</li>
                                    </ul>

                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                    <ul class="list-unstyled info-desc">
                                        <li class="d-flex">
                                            <strong>Weight </strong>
                                            <span>400 g</span>
                                        </li>
                                        <li class="d-flex">
                                            <strong>Dimensions </strong>
                                            <span>10 x 10 x 15 cm</span>
                                        </li>
                                        <li class="d-flex">
                                            <strong>Materials</strong>
                                            <span >60% cotton, 40% polyester</span>
                                        </li>
                                        <li class="d-flex">
                                            <strong>Color </strong>
                                            <span>Blue, Gray, Green, Red, Yellow</span>
                                        </li>
                                        <li class="d-flex">
                                            <strong>Size</strong>
                                            <span>L, M, S, XL, XXL</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <div class="media review-block mb-4">
                                                <img src="assets/images/avater-1.jpg" alt="reviewimg" class="img-fluid mr-4" />
                                                <div class="media-body">
                                                    <div class="product-review">
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                    </div>
                                                    <h6>Therichpost <span class="text-sm text-muted font-weight-normal ml-3">-June 23, 2019</span></h6>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum suscipit consequuntur in, perspiciatis laudantium ipsa fugit. Iure esse saepe error dolore quod.</p>
                                                </div>
                                            </div>

                                            <div class="media review-block">
                                                <img src="assets/images/avater-2.jpg" alt="reviewimg" class="img-fluid mr-4" />
                                                <div class="media-body">
                                                    <div class="product-review">
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star"></i></span>
                                                        <span><i class="tf-ion-android-star-outline"></i></span>
                                                    </div>
                                                    <h6>Therichpost <span class="text-sm text-muted font-weight-normal ml-3">-June 23, 2019</span></h6>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum suscipit consequuntur in, perspiciatis laudantium ipsa fugit. Iure esse saepe error dolore quod.</p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-5">
                                            <div class="review-comment mt-5 mt-lg-0">
                                                <h4 class="mb-3">Add a Review</h4>


                                                <div class="starrr"></div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Your Name" />
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" placeholder="Your Email" />
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="comment" id="comment" class="form-control" cols="30" rows="4" placeholder="Your Review"></textarea>
                                                </div>

                                                <a href="# " routerLink="/product-single" class="btn btn-main btn-small">Submit Review</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="products related-products section">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="title text-center">
                                <h2>You may like this</h2>
                                <p>The best Online sales to shop these weekend</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {
                            productList.map(
                                product =>
                                    <div class="col-lg-3 col-6" >
                                        <div class="product">
                                            <div class="product-wrap">
                                                <Link to={`/product-details/${product.iId}`} ><img class="img-fluid w-100 mb-3 img-first" src={product.productCoverImage} alt={product.productName} /></Link>
                                                <Link to={`/product-details/${product.iId}`} ><img class="img-fluid w-100 mb-3 img-second" src={product.productCoverImage} alt={product.productName} /></Link>
                                            </div>

                                            <span class="onsale">Sale</span>
                                            <div class="product-hover-overlay">
                                                <Link onClick={() => handleAddToCart(product)}><i class="tf-ion-android-cart"></i></Link>
                                                <a href="# " ><i class="tf-ion-ios-heart"></i></a>
                                            </div>

                                            <div class="product-info">
                                                <h2 class="product-title h5 mb-0"><Link to={`/product-details/${product.iId}`} >{product.productName}</Link></h2>
                                                <span class="price">
                                                    ₹{product.offerPrice} <del>₹{product.salePrice}</del>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                            )
                        }

                    </div>
                </div>
            </section>
        </div>
    );
}


export default ProductDetails;