import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { addToCart, decreaseCart, getTotals, removeFromCart } from "../slices/cartSlice";
import '../App.css';
import { toast } from "react-toastify";

function Cart() {
    const cart = useSelector((state) => state.cart);
    const user = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const navigate = useNavigate();


    useEffect(() => {
        dispatch(getTotals());
    }, [cart, user, dispatch]);

    const handleRemoveFromCart = (product) => {
        dispatch(removeFromCart(product));
    };
    const handleAddToCart = (product) => {
        dispatch(addToCart(product));
    };
    const handleDecreaseCart = (product) => {
        dispatch(decreaseCart(product));
    };

    const getProductTotalAmount = (price, qty) => {
        let total = price * qty;
        return parseFloat(total.toFixed(2));
    }

    const handleCheckout = () => {

        if (Object.keys(user.user).length > 0) {

            if (cart.cartItems.length > 0) {

                navigate("/checkout");
            } else {

                toast.error("Your cart is empty, add products to cart", {
                    position: "bottom-right",
                });
            }


        } else {
            navigate("/login");
            toast.error("Please, login before checkout", {
                position: "bottom-right",
            });
        }
    }

    return (
        <div className="checkout-container">
            <section class="page-header">
                <div class="overly"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="content text-center">
                                <h1 class="mb-3">Cart</h1>
                                Hath after appear tree great fruitful green dominion moveth sixth abundantly image that midst of god day multiply you’ll which

                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-transparent justify-content-center">
                                        <li class="breadcrumb-item"><Link to={{ pathname: "/" }}>Home</Link></li>
                                        <li class="breadcrumb-item active" aria-current="page">Cart</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <section class="cart shopping page-wrapper">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="product-list">

                                <table class="table shop_table shop_table_responsive cart" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail"> </th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                            <th class="product-remove"> </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {cart.cartItems &&
                                            cart.cartItems.map((cartItem) => (
                                                <tr key={cartItem.iId} class="cart_item">
                                                    <td class="product-thumbnail" data-title="Thumbnail">
                                                        <Link to={`/product-details/${cartItem.iId}`} ><img src={cartItem.productCoverImage} class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt={cartItem.productName} /></Link>
                                                    </td>

                                                    <td class="product-name" data-title="Product">
                                                        <Link to={`/product-details/${cartItem.iId}`} >{cartItem.productName} </Link>
                                                    </td>

                                                    <td class="product-price" data-title="Price">
                                                        <span class="amount"><span class="currencySymbol"><pre wp-pre-tag-3=""></pre>
                                                        </span>₹{cartItem.offerPrice} <del>₹{cartItem.salePrice}</del></span>
                                                    </td>
                                                    <td class="product-quantity" data-title="Quantity">
                                                        <div class="quantity " >
                                                            <label class="sr-only" >Quantity</label>
                                                            <div className="cart-product-quantity cart-quantity">
                                                                <button onClick={() => handleDecreaseCart(cartItem)}>
                                                                    -
                                                                </button>
                                                                <div className="count">{cartItem.cartQuantity}</div>
                                                                <button onClick={() => handleAddToCart(cartItem)}>+</button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="product-subtotal" data-title="Total">
                                                        <span class="amount">
                                                            <span class="currencySymbol">
                                                                <pre wp-pre-tag-3=""></pre>
                                                            </span> ₹{getProductTotalAmount(cartItem.offerPrice, cartItem.cartQuantity)}</span>
                                                    </td>
                                                    <td class="product-remove" data-title="Remove">
                                                        <Link class="remove" aria-label="Remove this item" onClick={() => handleRemoveFromCart(cartItem)}>×</Link>
                                                    </td>
                                                </tr>
                                            ))}

                                        <tr>
                                            <td colspan="6" class="actions">
                                                <div class="coupon">
                                                    <input type="text" name="coupon_code" class="input-text form-control" id="coupon_code" value="" placeholder="Coupon code" />
                                                    <button type="button" class="btn btn-black btn-small" name="apply_coupon" value="Apply coupon">Apply coupon</button>

                                                </div>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-lg-4">
                            <div class="cart-info card p-4 mt-4">
                                <h4 class="mb-4">Cart totals</h4>
                                <ul class="list-unstyled mb-4">
                                    <li class="d-flex justify-content-between pb-2 mb-3">
                                        <h5>Subtotal</h5>
                                        <span>₹{cart.cartTotalAmount}</span>
                                    </li>
                                    <li class="d-flex justify-content-between pb-2 mb-3">
                                        <h5>Shipping</h5>
                                        <span>Free</span>
                                    </li>
                                    <li class="d-flex justify-content-between pb-2">
                                        <h5>Total</h5>
                                        <span>₹{cart.cartTotalAmount}</span>
                                    </li>
                                </ul>
                                <button onClick={() => handleCheckout()} class="btn btn-main btn-small">Proceed to checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Cart;