
import React, { useEffect } from 'react';

import "jquery/dist/jquery.slim.min.js";
import "popper.js/dist/umd/popper.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { userLogout } from '../slices/userSlice';
import { getTotals, removeFromCart } from '../slices/cartSlice';
function Header() {

    const cart = useSelector((state) => state.cart);
    const user = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(getTotals());
    }, [user, cart, dispatch]);


    const handleUserLogout = () => {
        dispatch(userLogout());
        navigate("/");
    };

    const handleRemoveFromCart = (product) => {
        dispatch(removeFromCart(product));
    };

    return (<nav class="navbar navbar-expand-lg navbar-light bg-white w-100 navigation" id="navbar">
        <div class="container">
            <Link class="navbar-brand font-weight-bold" to={{ pathname: "/" }}>Shopping-Cart</Link>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
                aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="main-navbar">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active">
                        <Link class="nav-link" to={{ pathname: "/" }}>Home</Link>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="# " >About Us</a>
                    </li>

                    <li class="nav-item dropdown dropdown-slide">
                        <a class="nav-link dropdown-toggle" href="# " id="navbarDropdown3" role="button" data-delay="350"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Shop.
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                            <li><Link to={{ pathname: "/shop" }}>Shop</Link></li>

                            <li><Link to={{ pathname: "/cart" }}>Cart</Link></li>

                        </ul>
                    </li>

                    <li class="nav-item dropdown dropdown-slide">
                        <a class="nav-link dropdown-toggle" href="# " id="navbarDropdown5" role="button" data-delay="350"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Account.
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown5">

                            {
                                user.user.length === 0 ? (
                                    <>
                                        <li><Link to={{ pathname: "/login" }}>Login</Link></li>
                                        <li><Link to={{ pathname: "/signup" }}>SignUp</Link></li>
                                        <li><Link to={{ pathname: "/forgot-password" }}>Forgot Password</Link></li>
                                    </>
                                ) : (
                                    <>
                                        <li><Link to={{ pathname: "/my-order-list" }}>My Orders</Link></li>
                                        <li><a href="# " onClick={() => handleUserLogout()}>Logout</a></li>

                                    </>
                                )
                            }



                        </ul>
                    </li>
                </ul>
            </div>

            <ul class="top-menu list-inline mb-0 d-none d-lg-block" id="top-menu">
                <li class="list-inline-item">
                    <a href="# " class="search_toggle" id="search-icon"><i class="tf-ion-android-search"></i></a>
                </li>
                <li class="dropdown cart-nav dropdown-slide list-inline-item">
                    <a href="# " class="dropdown-toggle cart-icon" data-toggle="dropdown" data-hover="dropdown">
                        <i class="tf-ion-android-cart"></i><button className="badge">{cart.cartItems.length}</button>
                    </a>
                    <div class="dropdown-menu cart-dropdown">

                        {cart.cartItems &&
                            cart.cartItems.map((cartItem) => (
                                <div key={cartItem.iId} class="media">
                                    <Link to={`/product-details/${cartItem.iId}`} >
                                        <img class="media-object img- mr-3" src={cartItem.productCoverImage} alt={cartItem.productName} />
                                    </Link>
                                    <div class="media-body">
                                        <h6>{cartItem.productName}</h6>
                                        <div class="cart-price">
                                            <span>{cartItem.cartQuantity} x</span>
                                            <span> {cartItem.offerPrice}</span>
                                        </div>
                                    </div>
                                    <a href="# " class="remove" onClick={() => handleRemoveFromCart(cartItem)}><i class="tf-ion-close"></i></a>
                                </div>
                            ))}



                        <div class="cart-summary">
                            <span class="h6">Total</span>
                            <span class="total-price h6">₹{cart.cartTotalAmount}</span>
                            <div class="text-center cart-buttons mt-3">
                                <a href="# " class="btn btn-small btn-transparent btn-block" ><Link to={{ pathname: "/cart" }}>View Cart</Link></a>
                                {
                                    user.user.length !== 0 ? (
                                        <a href="# " class="btn btn-small btn-main btn-block"><Link to={{ pathname: "/checkout" }}>Checkout</Link></a>
                                    ) : (
                                        ''
                                    )}
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-inline-item"><a href="# " ><i class="tf-ion-ios-person mr-3"></i></a></li>
            </ul>
        </div>
    </nav>);
}

export default Header;