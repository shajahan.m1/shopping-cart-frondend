import React, { useEffect } from "react";
import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import OrderService from "../Services/OrderService";
import { clearCart } from "../slices/cartSlice";

function Checkout() {

    const cart = useSelector((state) => state.cart);
    const user = useSelector((state) => state.user);

    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {

    }, [cart, user]);



    const deliveryAddress = useRef()
    const contactNo = useRef()
    const orderNote = useRef()

    const handleSubmit = (e) => {
        e.preventDefault();
        if (Object.keys(user.user).length > 0) {
            if (cart.cartItems.length > 0 && cart.cartTotalAmount > 0) {
                let orderInput = { cartItems: cart.cartItems, orderAmount: cart.cartTotalAmount, user: user.user, deliveryAddress: deliveryAddress.current.value, contactNo: contactNo.current.value, orderNote: orderNote.current.value };
                OrderService.placeOrder(orderInput).then((res) => {
                    if (res.data !== null && res.data !== "") {

                        navigate("/my-order-list");
                        toast.info("Succesfully Order Placed", {
                            position: "bottom-right",
                        });
                        dispatch(clearCart());
                    } else {
                        toast.error("Order creation failed", {
                            position: "bottom-right",
                        });
                    }
                });
            } else {

                toast.error("Your cart is empty, add products to cart", {
                    position: "bottom-right",
                });
            }
        } else {
            navigate("/login");
            toast.error("Please, login before checkout", {
                position: "bottom-right",
            });
        }

    }

    return (
        <div className="checkout-container">
            <section class="page-header">
                <div class="overly"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="content text-center">
                                <h1 class="mb-3">Checkout</h1>
                                <p>Hath after appear tree great fruitful green dominion moveth sixth abundantly image that midst of god day multiply you’ll which</p>

                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-transparent justify-content-center">
                                        <li class="breadcrumb-item"><Link to={{ pathname: "/" }}>Home</Link></li>
                                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="page-wrapper">
                <div class="checkout shopping">
                    <div class="container">
                        <form class="checkout-form" onSubmit={handleSubmit}>
                            <div class="row">
                                <div class="col-lg-8 pr-5">


                                    <div class="billing-details mt-5">
                                        <h4 class="mb-4">Delivery Details</h4>

                                        <div class="row">


                                            <div class="col-lg-12">
                                                <div class="form-group mb-4">
                                                    <label for="first_name">Delivery Address</label>
                                                    <input type="text" class="form-control" id="street" placeholder="Delivery Address" ref={deliveryAddress} required />
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group mb-4">
                                                    <label for="first_name">Contact No</label>
                                                    <input type="text" class="form-control" id="phone" value={user.user.contactNo} placeholder="Contact No" ref={contactNo} required />
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <div class="form-group mb-4">
                                                    <label for="first_name">Order notes (optional)</label>
                                                    <textarea class="form-control" id="msg" cols="30" rows="5" placeholder="Notes about order e:g: want to say something" ref={orderNote}></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-6 col-lg-4">
                                    <div class="product-checkout-details mt-5 mt-lg-0">
                                        <h4 class="mb-4 border-bottom pb-4">Order Summary</h4>
                                        {cart.cartItems &&
                                            cart.cartItems.map((cartItem) => (
                                                <div key={cartItem.iId} class="media product-card">
                                                    <p>{cartItem.productName}</p>
                                                    <div class="media-body text-right">
                                                        <p class="h5">{cartItem.cartQuantity} x ₹{cartItem.offerPrice}</p>
                                                    </div>
                                                </div>

                                            ))}


                                        <ul class="summary-prices list-unstyled mb-4">
                                            <li class="d-flex justify-content-between">
                                                <span >Subtotal:</span>
                                                <span class="h5">₹{cart.cartTotalAmount}</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <span >Shipping:</span>
                                                <span class="h5">Free</span>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <span>Total</span>
                                                <span class="h5">₹{cart.cartTotalAmount}</span>
                                            </li>
                                        </ul>

                                        <div class="form-check mb-3">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked />
                                            <label class="form-check-label" for="exampleRadios1">
                                                COD
                                            </label>

                                        </div>
                                        <button type="submit" class="btn btn-main btn-small">Place Order</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Checkout;