import React, { useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import UserService from "../Services/UserService";

function Signup() {

    const navigate = useNavigate();

    const firstName = useRef()
    const lastName = useRef()
    const contactNo = useRef()
    const email = useRef()
    const password = useRef()

    const handleSubmit = (e) => {
        e.preventDefault();
        let userInput = { firstName: firstName.current.value, lastName: lastName.current.value, contactNo: contactNo.current.value, email: email.current.value, password: password.current.value };
        UserService.userSignin(userInput).then((res) => {
            if (res.data !== null && res.data !== "") {
                navigate("/login");
                toast.info("Succesfully registered", {
                    position: "bottom-right",
                });
            } else {
                toast.error("User registration failed", {
                    position: "bottom-right",
                });
            }
        });

    }

    return (
        <div className="signUp-container">
            <div class="account section">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="login-form border p-5">
                                <div class="text-center heading">
                                    <h2 class="mb-2">Sign Up</h2>
                                    <p class="lead">Already have an account? <Link to={{ pathname: "/login" }}> Login now</Link></p>
                                </div>

                                <form onSubmit={handleSubmit} >
                                    <div class="form-group mb-4">
                                        <label for="# " >First Name</label>
                                        <input type="text" class="form-control" placeholder="Enter First Name" ref={firstName} required />
                                    </div>
                                    <div class="form-group mb-4">
                                        <label for="# " >Last Name</label>
                                        <input type="text" class="form-control" placeholder="Enter Last Name" ref={lastName} required />
                                    </div>
                                    <div class="form-group mb-4">
                                        <label for="# " >Contact No</label>
                                        <input type="number" class="form-control" placeholder="Enter Contact No" ref={contactNo} required />
                                    </div>
                                    <div class="form-group mb-4">
                                        <label for="# " >Email (Username)</label>
                                        <input type="email" class="form-control" placeholder="Enter Email" ref={email} required />
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="# " > Password</label>
                                        <input type="password" class="form-control" placeholder="Enter Password" ref={password} required />
                                    </div>

                                    <button class="btn btn-main mt-3 btn-block">Signup</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Signup;