import React, { useRef } from "react";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import UserService from "../Services/UserService";
import { addToSession } from "../slices/userSlice";

function Login() {

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const email = useRef()
    const password = useRef()

    const handleSubmit = (e) => {
        e.preventDefault();
        UserService.userLogin(email.current.value, password.current.value).then((res) => {
            if (res.data !== null && res.data !== "") {
                dispatch(addToSession(res.data));

                navigate("/");
            } else {
                toast.error("Invalid username/password", {
                    position: "bottom-right",
                });
            }
        });

    }

    return (
        <div className="login-container">
            <div class="account section">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="login-form border p-5">
                                <div class="text-center heading">
                                    <h2 class="mb-2">Login</h2>
                                    <p class="lead">Don’t have an account? <Link to={{ pathname: "/signup" }}>Create a free account</Link></p>
                                </div>

                                <form onSubmit={handleSubmit}>
                                    <div class="form-group mb-4">
                                        <label for="# " >Enter username (Email)</label>
                                        <input type="email" class="form-control" placeholder="Enter Username" ref={email} required />
                                    </div>
                                    <div class="form-group">
                                        <label for="# " >Enter Password</label>
                                        <Link to={{ pathname: "/forgot-password" }} class="float-right" >Forgot password?</Link>
                                        <input type="password" class="form-control" placeholder="Enter Password" ref={password} />
                                    </div>

                                    <button class="btn btn-main mt-3 btn-block" type="submit">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default Login;