import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import OrderService from "../Services/OrderService";


function MyOrderList() {

    const user = useSelector((state) => state.user);
    const [orderList, setOrderList] = useState([])

    useEffect(() => {
        getMyOrderList()
    }, [user]);

    const getMyOrderList = () => {
        OrderService.myOrderList(user.user.iId).then((res) => {
            setOrderList(res.data)
        });
       
    };


    return (<div className="checkout-container">
        <section class="page-header">
            <div class="overly"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="content text-center">
                            <h1 class="mb-3">Cart</h1>
                            Hath after appear tree great fruitful green dominion moveth sixth abundantly image that midst of god day multiply you’ll which

                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb bg-transparent justify-content-center">
                                    <li class="breadcrumb-item"><Link to={{ pathname: "/" }}>Home</Link></li>
                                    <li class="breadcrumb-item active" aria-current="page">My Orders</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="cart shopping page-wrapper">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="product-list">

                            <table class="table shop_table shop_table_responsive cart" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="product-thumbnail"> #</th>
                                        <th class="product-name">Order No</th>
                                        <th class="product-price">Order Date</th>
                                        <th class="product-quantity">Order Amount</th>
                                        <th class="product-quantity">Delivery Address</th>
                                        <th class="product-quantity">Contact No</th>
                                        <th class="product-quantity">Order Note</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {orderList &&
                                        orderList.map((order,index) => (
                                            <tr key={order.iId} class="cart_item">
                                                <td class="product-thumbnail" data-title="Thumbnail">
                                                {index+1}
                                                </td>

                                                <td class="product-name" data-title="Product">
                                                {order.orderNo}
                                                </td>

                                                <td class="product-price" data-title="Price">
                                                    
                                                    <span>{order.orderDate}</span>
                                                </td>
                                                <td class="product-quantity" data-title="Quantity">
                                                   <span>{order.orderAmount}</span>
                                                </td>
                                                <td class="product-quantity" data-title="Quantity">
                                                   <span>{order.deliveryAddress}</span>
                                                </td>
                                                <td class="product-quantity" data-title="Quantity">
                                                   <span>{order.contactNo}</span>
                                                </td>
                                                <td class="product-quantity" data-title="Quantity">
                                                   <span>{order.orderNote}</span>
                                                </td>

                                            </tr>
                                        ))}

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>)

}

export default MyOrderList;