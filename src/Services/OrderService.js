import axios from "axios";


const PLACE_ORDER_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/place-order";
const MY_ORDER_LIST_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/my-order-list";

class OrderService {
    placeOrder(orderInput) {
        return axios.post(PLACE_ORDER_API_BASE_URL, orderInput);
    }

    myOrderList(userId) {
        return axios.get(MY_ORDER_LIST_API_BASE_URL + '?userId=' + userId);
    }
}


export default new OrderService()