import axios from "axios";

const PRODUCT_LIST_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/product";
const PRODUCT_INFO_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/product-info";

class ProductService {
    getProductList() {
        return axios.get(PRODUCT_LIST_API_BASE_URL);
    }

    getProductInfoById(productId) {
        return axios.get(PRODUCT_INFO_API_BASE_URL + '?productId=' + productId);
    }

}

export default new ProductService()