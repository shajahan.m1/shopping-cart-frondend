import axios from "axios";

const CATEGORY_LIST_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/category";

class CategoryService{
    getCategoryList(){
        return axios.get(CATEGORY_LIST_API_BASE_URL);
    }
}

export default new CategoryService()