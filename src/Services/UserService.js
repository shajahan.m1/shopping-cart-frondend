import axios from "axios";

const USER_LOGIN_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/user-login";
const USER_SIGNIN_API_BASE_URL = "https://shopping-cart-admin.azurewebsites.net/api/customer/user-register";

class UserService {
    userLogin(email, password) {
        return axios.get(USER_LOGIN_API_BASE_URL + "?email=" + email + "&password=" + password);
    }

    userSignin(userInput) {
        return axios.post(USER_SIGNIN_API_BASE_URL, userInput);
    }
}

export default new UserService()