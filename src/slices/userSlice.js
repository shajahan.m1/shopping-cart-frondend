import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

const initialState = {
    user: localStorage.getItem("user")
        ? JSON.parse(localStorage.getItem("user"))
        : []
};

const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        addToSession(state, action) {


            state.user = { ...action.payload };

            localStorage.setItem("user", JSON.stringify(state.user));

            toast.info("Successfully Logged In", {
                position: "bottom-right",
            });
        },
        userLogout(state, action) {
            state.user = [];
            localStorage.setItem("user", JSON.stringify(state.user));
            toast.info("Succesfully Logout", { position: "bottom-right" });
        },
    },
});

export const { userLogout, addToSession } =
    userSlice.actions;

export default userSlice.reducer;